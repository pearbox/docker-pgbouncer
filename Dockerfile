ARG baseImage
# hadolint ignore=DL3006
FROM $baseImage AS build

LABEL MAINTAINER "Artyom Nosov <chip@unixstyle.ru>"

# hadolint ignore=DL3018
RUN apk --no-cache add \
      autoconf \
      autoconf-doc \
      automake \
      c-ares-dev \
      curl \
      file \
      gcc \
      libc-dev \
      libevent-dev \
      libressl-dev \
      libtool \
      make \
      pkgconf

ARG pgbouncerVersion

WORKDIR /build

# hadolint ignore=DL3003
RUN set -x \
    && curl -ssL "https://pgbouncer.github.io/downloads/files/${pgbouncerVersion}/pgbouncer-${pgbouncerVersion}.tar.gz" -o pgbouncer.tar.gz \
    && tar -zxC /build -f pgbouncer.tar.gz \
    && cd pgbouncer-${pgbouncerVersion} \
    && ./configure \
    && make \
    && make install \
    && find /usr/local -type f

ARG baseImage
# hadolint ignore=DL3006
FROM $baseImage

ENV PGBOUNCER_USER postgres

# https://git.alpinelinux.org/aports/tree/main/postgresql/postgresql.pre-install?h=3.11-stable
# Fixed GID/UID values as this users was previously included in the
# default /etc/passwd as shipped by main/alpine-baselayout.

RUN set -x \
    && addgroup -g 70 -S postgres \
    && adduser -u 70 -S -D -G postgres -H -h /var/lib/postgresql -s /bin/sh postgres

# hadolint ignore=DL3018
RUN set -x \ 
    && apk --no-cache add \
        c-ares \
        ca-certificates \
        libevent \
        libressl \
        openssl \
    && mkdir /etc/pgbouncer /var/log/pgbouncer \
    && chown postgres:postgres /var/log/pgbouncer \
    && touch /etc/pgbouncer/pgbouncer.ini /etc/pgbouncer/userlist.txt \
    && chown root:postgres /etc/pgbouncer/pgbouncer.ini /etc/pgbouncer/userlist.txt \
    && chmod 0640 /etc/pgbouncer/pgbouncer.ini /etc/pgbouncer/userlist.txt 

COPY --from=build /usr/local/bin/pgbouncer /usr/local/bin/pgbouncer
COPY docker-entrypoint.sh /usr/local/bin

ENTRYPOINT [ "docker-entrypoint.sh" ]
